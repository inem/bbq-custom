# (с) goodprogrammer.ru
#
# Контроллер вложенного ресурса подписок
class SubscriptionsController < ApplicationController
  # Задаем «родительский» event для подписки
  before_action :set_event, only: [:create, :destroy]

  # Задаем подписку, которую юзер хочет удалить
  before_action :set_subscription, only: [:destroy]


  def create
    if current_user == @event.user
      flash[:alert] =  I18n.t('controllers.subscriptions.same_user_error')
      render 'events/show'
    else
      @new_subscription = if current_user
        @event.subscriptions.build(subscription_params.merge(user: current_user))
      else
        @event.subscriptions.find_or_initialize()
      end

      if @new_subscription.save
        # Если сохранилась успешно, редирект на страницу самого события
        redirect_to @event, notice: I18n.t('controllers.subscriptions.created')
      else
        # если ошибки — рендерим здесь же шаблон события
        render 'events/show', alert: I18n.t('controllers.subscriptions.error')
      end
    end
  end

  def destroy
    message = {notice: I18n.t('controllers.subscriptions.destroyed')}

    if current_user_can_edit?(@subscription)
      @subscription.destroy
    else
      message = {alert: I18n.t('controllers.subscriptions.error')}
    end

    redirect_to @event, message
  end

  private
  def set_subscription
    @subscription = @event.subscriptions.find(params[:id])
  end

  def set_event
    @event = Event.find(params[:event_id])
  end

  def subscription_params
    # .fetch разрешает в params отсутствие ключа :subscription
    params.fetch(:subscription, {}).permit(:user_email, :user_name)
  end
end
